defmodule UniqueKeys do
  @moduledoc """
  UniqueKeys is a set of utilities to create unique universal and domain keys.  It uses crypto strength utilities combined with base 36 encoded keys.
  """

  alias UniqueKeys.Keys

  @doc """
  Show the current version
  """
  def version() do
    {:ok, vsn} = :application.get_key(:unique_keys, :vsn)
    List.to_string(vsn)
  end

  @doc """
  Creates a time-based unique `key` safe to use for domain specific identifiers.  A domain may be a user, booking, etc. This key can be decoded using decode_key/1 to extract the timestamp.  The key is created from a timestamp plus 13 random characters. The integer value is then converted to base 64 and returned as a string.

  Returns the 20 character binary/string key.

  ## Examples

    > UniqueKeys.create_key()
    "477BGCDXGNK3ZLAVNTDF"

  """
  defdelegate create_key(), to: Keys

  @doc """
  Decodes a time-based unique `key` to extract the timestamp.

  Returns the timestamp

  ## Examples

    > UniqueKeys.decode_key("477BGCDXGNK3ZLAVNTDF")
    {:ok, #DateTime<2019-06-03 15:14:45.694130Z>}

  """
  defdelegate decode_key(key), to: Keys

  @doc """
  Creates a routable, sharded domain key where the first character is the route and the second is a equally distributed shard key with 36 slots.  The key is created from a timestamp plus 13 random characters. The integer value is then converted to base 64 and returned as a string.

  ## Examples

    # where the route is `2` and the shard is generated (0..Z):
    > UniqueKeys.shard_key(2)
    "2W477BWWVSFT6W2YLUD41K"

  """
  defdelegate shard_key(route, shard \\ nil), to: Keys

  @doc """
  Decode a shard key where to extract the route, shard and timestampe

  ## Examples

    > UniqueKeys.decode_shard_key("2W477BWWVSFT6W2YLUD41K")
    {:ok, [route: "2", shard: "W"], #DateTime<2019-06-03 16:15:48.742358Z>}

  """
  defdelegate decode_shard_key(key), to: Keys

  @doc """
  Creates a short domain key with a timestamp and 2 character random.  The integer value is then converted to base64.

  ## Examples

    > UniqueKeys.create_txid()
    "www9dga1e9pwi"

  """
  defdelegate create_txid(), to: Keys

  defdelegate decode_txid(key), to: Keys

  defdelegate create_uuid(), to: Keys

  defdelegate create_guid(), to: Keys

  defdelegate create_bytes(size \\ 24, base \\ 36), to: Keys
end
