defmodule UniqueKeys.Keys do
  @base 36
  @min 1_000_000_000_000
  @max 9_999_999_999_999

  @spec create_key() :: <<_::200>>
  def create_key() do
    key = :os.system_time(:nanosecond) * @min + :rand.uniform(@max)
    Integer.to_string(key, @base)
  end

  @spec decode_key(binary()) :: {:error, :incompatible_calendars | :invalid_unix_time} | {:ok, DateTime.t()}
  def decode_key(key) when is_binary(key) do
    String.to_integer(key, @base)
    |> div(@min * 1000)
    |> DateTime.from_unix(:nanosecond)
  end

  defp sharded_key(route, shard), do: Enum.join([route, shard, create_key()], "")

  @doc """
  create a key suitable for shard routing where the first character is passed in 
  and the second is a random generated route.
  """
  def shard_key(route, shard) when shard == nil do
    # :rand.uniform returns 1..n but we need 0..35 to keep to our range of 0..Z
    shard = (:rand.uniform(@base) - 1) |> Integer.to_string(@base)
    sharded_key(route, shard)
  end

  def shard_key(route, shard) when shard != nil, do: sharded_key(route, shard)

  def decode_shard_key(key) do
    points = key |> String.codepoints()
    {route, points} = {hd(points), tl(points)}
    {shard, points} = {hd(points), tl(points)}
    {:ok, ts} = points |> Enum.join() |> decode_key()

    {:ok, [route: route, shard: shard], ts}
  end

  @spec create_txid() :: binary()
  def create_txid() do
    key = :os.system_time(:nanosecond) * 100 + :rand.uniform(100)
    Integer.to_string(key, @base) |> String.downcase()
  end

  @spec decode_txid(any()) ::
          {:error, :incompatible_calendars | :invalid_unix_time} | {:ok, DateTime.t()}
  def decode_txid(key) do
    String.to_integer(key, @base)
    |> div(100_000)
    |> DateTime.from_unix(:nanosecond)
  end

  def create_guid() do
    create_bytes(16, 16)
  end

  # does not comply with any RFC standards
  def create_uuid() do
    create_bytes(16, 16) |> format_uuid()
  end

  defp format_uuid(bytes) when is_binary(bytes) do
    bytes |> String.codepoints() |> format_uuid()
  end

  defp format_uuid(bytes) when is_list(bytes) do
    [
      Enum.slice(bytes, 0, 8),
      Enum.slice(bytes, 8, 4),
      Enum.slice(bytes, 12, 4),
      Enum.slice(bytes, 16, 4),
      Enum.slice(bytes, 20, 12)
    ]
    |> Enum.join("-")
  end

  def create_bytes(size, base) do
    :crypto.strong_rand_bytes(size)
    |> :binary.bin_to_list()
    |> Enum.map(fn n -> int_to_string(n, base) end)
    |> Enum.join("")
    |> String.downcase()
  end

  defp int_to_string(n, base, pad \\ 2) do
    Integer.to_string(n, base) |> String.pad_leading(pad, "0")
  end

  def red(text) do
    Enum.join([IO.ANSI.red(), text, IO.ANSI.reset()], "")
  end

  def green(text) do
    Enum.join([IO.ANSI.green(), text, IO.ANSI.reset()], "")
  end
end
