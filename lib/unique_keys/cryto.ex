defmodule UniqueKeys.Crypto do
  @byte_size 16
  @aad "AES128GCM"

  use Bitwise

  require Logger

  def generate_key(byte_size \\ @byte_size) do
    :crypto.strong_rand_bytes(byte_size)
  end

  def generate_keystore(count \\ 10_000, byte_size \\ @byte_size) do
    for _ <- 1..count do
      :crypto.strong_rand_bytes(byte_size)
    end
  end

  @doc """
  Create a single key as an xor product of the two primary keys.
  """
  def create_key(key1, key2) do
    n1 = :crypto.bytes_to_integer(key1)
    n2 = :crypto.bytes_to_integer(key2)

    Bitwise.bxor(n1, n2) |> Integer.digits(256) |> :erlang.list_to_bitstring()
  end

  @doc """
  Pull the digits from the supplied integer in byte sizes and return the bitstring.  Add padding to the left side if required by the size param.
  """
  def integer_to_bitstring(number, size) do
    digits = Integer.digits(number, 256)

    pad = for _ <- 1..(size - Enum.count(digits)), do: 0

    [pad | digits] |> List.flatten() |> :erlang.list_to_bitstring()
  end

  @doc """
  Generate a set of index to use in the keystore.  Return the generated 4 byte bitstring, index1 and index2 as a single tuple.
  """
  def generate_index() do
    bytes = :crypto.strong_rand_bytes(4)

    {idx1, idx2} = index_from_bytes(bytes)

    {bytes, idx1, idx2}
  end

  @doc """
  Pull index 1 and 2 from the bitstring.  Return {idx1, idx2}
  """
  def index_from_bytes(bytes, size \\ 10_000) do
    n = :crypto.bytes_to_integer(bytes)
    {rem(n, size), div(n, size) |> rem(size)}
  end

  @doc """
  Encrypt the plain text using keys from the keystore and return {:ok, cipher_text} on success, {:error, reason} on fail.
  """
  def encrypt(plain_text, keystore) when is_list(keystore) do
    {bytes, idx1, idx2} = generate_index()

    k1 = Enum.at(keystore, idx1)
    k2 = Enum.at(keystore, idx2)
    k3 = create_key(k1, k2)

    encrypt(plain_text, k3, bytes)
  end

  @doc """
  Encrypt the plain text string (binary) using the raw key (bitstring).  Return the raw encrypted bitstring as {:ok, cipher_text}
  """
  def encrypt(plain_text, key, index_bytes) when is_bitstring(key) do
    mode = :aes_128_gcm
    iv = :crypto.strong_rand_bytes(@byte_size)

    {cipher_text, cipher_tag} = :crypto.crypto_one_time_aead(mode, key, iv, plain_text, @aad, true)

    enc = index_bytes <> iv <> cipher_tag <> cipher_text

    {:ok, enc}
  end

  @doc """
  Dencrypt the cipher_text string (bitstring) using the raw key (bitstring).  Return the plain text as {:ok, plain_text} or {:error, reason} on fail.
  """
  def decrypt(cipher_text, keystore) when is_list(keystore) do
    <<index_bytes::binary-4, iv::binary-16, tag::binary-16, cipher_text::binary>> = cipher_text

    {idx1, idx2} = index_from_bytes(index_bytes)

    k1 = Enum.at(keystore, idx1)
    k2 = Enum.at(keystore, idx2)
    k3 = create_key(k1, k2)

    mode = :aes_128_gcm
    plain_text = :crypto.crypto_one_time_aead(mode, k3, iv, cipher_text, @aad, tag, false)

    {:ok, plain_text}
  end
end
