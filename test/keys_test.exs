defmodule KeysTest do
  use ExUnit.Case
  doctest UniqueKeys.Keys

  alias UniqueKeys.Keys

  describe "create_key/0" do
    test "should create a key with a specified length" do
      key = Keys.create_key()
      assert String.length(key) == 20
    end

    test "min and max tests" do
      min =
        (DateTime.to_unix(~U[2020-01-01 01:02:03.000001Z], :nanosecond) * 1_000_000_000_000) |> Integer.to_string(36)

      assert String.length(min) == 20

      max =
        (DateTime.to_unix(~U[2099-12-31 01:02:03.000009Z], :nanosecond) * 1_000_000_000_000 + 9_999_999_999_999)
        |> Integer.to_string(36)

      assert String.length(max) == 20
    end

    test "should decode a standard key" do
      key = Keys.create_key()
      assert {:ok, dt} = Keys.decode_key(key)
      assert is_map(dt)
    end
  end

  describe "create_txid/0" do
    test "should create a txid" do
      txid = Keys.create_txid()
      assert String.length(txid) == 13
    end

    test "should decode the date time from a txid" do
      txid = Keys.create_txid()
      assert {:ok, dt} = Keys.decode_txid(txid)
      assert is_map(dt)
    end

    test "should not detect collisions over a large set" do
      iterations = 200_000

      count =
        Stream.repeatedly(fn -> Keys.create_txid() end)
        |> Stream.take(iterations)
        |> Enum.into(MapSet.new())
        |> Enum.count()

      assert count == iterations
    end
  end

  describe "shard_key/2" do
    test "single character" do
      for n <- 0..35 do
        str = Integer.to_string(n, 36)
        assert String.length(str) == 1
      end
    end

    test "should create a random shared key" do
      key = Keys.shard_key(1, nil)
      assert String.length(key) == 22
      assert key |> String.codepoints() |> Enum.at(0) == "1"

      key = Keys.shard_key("5", nil)
      assert String.length(key) == 22
      assert key |> String.codepoints() |> Enum.at(0) == "5"
    end

    test "should create a static sharded key" do
      key = Keys.shard_key(9, 4)
      assert String.length(key) == 22
      assert key |> String.codepoints() |> Enum.at(0) == "9"
      assert key |> String.codepoints() |> Enum.at(1) == "4"
    end

    test "red/1" do
      assert Keys.red("this is a red string") != nil
    end

    test "green/1" do
      assert Keys.green("this is a green string") != nil
    end
  end
end
