defmodule UniqueKeysTest do
  use ExUnit.Case
  doctest UniqueKeys

  describe "unique keys public API" do
    test "version/0" do
      assert version = UniqueKeys.version()
      assert is_binary(version)
      assert String.split(version, ".") |> Enum.count() == 3
    end

    test "should encode and decode a standard key" do
      key = UniqueKeys.create_key()
      assert String.length(key) == 20
      assert {:ok, dt} = UniqueKeys.decode_key(key)
      assert is_map(dt)
    end

    test "should encode and decode a txid" do
      txid = UniqueKeys.create_txid()
      assert String.length(txid) == 13
      assert {:ok, dt} = UniqueKeys.decode_txid(txid)
      assert is_map(dt)
    end

    test "should encode a uuid" do
      uuid = UniqueKeys.create_uuid()
      assert String.length(uuid) == 36
    end

    test "should encode a guid" do
      guid = UniqueKeys.create_guid()
      assert String.length(guid) == 32
    end

    test "should return a random byte string with a default length" do
      bytes = UniqueKeys.create_bytes()
      assert String.length(bytes) == 48
    end

    test "should encode and decode a random shard key" do
      key = UniqueKeys.shard_key(8)
      assert String.length(key) == 22
      assert key |> String.codepoints() |> Enum.at(0) == "8"
      assert {:ok, [route, shard], ts} = UniqueKeys.decode_shard_key(key)
      assert route == {:route, "8"}
      assert shard != nil
      assert ts > 0
    end

    test "should create a shard key" do
      key = UniqueKeys.shard_key(6, 9)
      assert String.length(key) == 22
      assert key |> String.codepoints() |> Enum.at(0) == "6"
      assert key |> String.codepoints() |> Enum.at(1) == "9"
    end
  end
end
