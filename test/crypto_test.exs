defmodule UniqueKeysCryptoTest do
  use ExUnit.Case
  doctest UniqueKeys.Crypto

  require Logger

  alias UniqueKeys.Crypto

  setup_all do
    %{
      keystore: Crypto.generate_keystore()
    }
  end

  describe "UniqueKeys.Crypto API" do
    test "generate_key/1" do
      assert Crypto.generate_key() != nil
    end

    test "generate_keystore/2" do
      list = Crypto.generate_keystore()
      assert is_list(list)
      assert list |> Enum.count() == 10_000
    end

    test "create_key/2" do
      key1 = Crypto.generate_key()
      key2 = Crypto.generate_key()
      key3 = Crypto.create_key(key1, key2)

      assert Crypto.create_key(key1, key3) == key2
      assert Crypto.create_key(key2, key3) == key1
    end

    test "integer_to_bitstring/2" do
      n = 10_000
      assert bstring = Crypto.integer_to_bitstring(n, 8)
      assert :crypto.bytes_to_integer(bstring) == n
    end

    test "generate_index/1" do
      assert {bytes, idx1, idx2} = Crypto.generate_index()
      assert is_bitstring(bytes)
      assert is_integer(idx1)
      assert is_integer(idx2)

      assert idx1 < 10_000
      assert idx2 < 10_000
    end

    test "encrypt/2", context do
      keystore = context.keystore
      plain_text = "This is my text in plain view for everyone to see"

      assert {:ok, cipher_text} = Crypto.encrypt(plain_text, keystore)
      Logger.info(inspect(cipher_text))

      # decrypt and check
      assert {:ok, text} = Crypto.decrypt(cipher_text, keystore)
      assert text != nil
    end

    test "decrypt/2", context do
      # get the cipher text from datafile...
      keystore = context.keystore
      {:ok, plain_text} = File.read("./README.md")

      {:ok, cipher_text} = Crypto.encrypt(plain_text, keystore)

      assert {:ok, text} = Crypto.decrypt(cipher_text, keystore)
      assert is_binary(text)
      assert text == plain_text
    end
  end
end
