# UniqueKeys

## Overview

UniqueKeys is a set of utilities to create unique universal and domain keys.  It uses crypto strength utilities for random number generation (RNG) combined with base 36 encoded keys.

## Domain Key

A domain key is appropriate for a specific set of domains, e.g., "user", "company", etc. A domain key is created with the command `UniqueKeys.createKey()` and returns a 20 character time and RNG key.  Keys can be decoded with `UniqueKeys.decode_key(key)` to reveal the creation date.

Example:

```
key = UniqueKeys.create_key()  
"477BGCDXGNK3ZLAVNTDF"

UniqueKeys.decode_key(key)
{:ok, #DateTime<2019-06-03 15:14:45.694130Z>}
```

## Shard Key

A shard key adds two special characters to a simple domain key.  The first character is used for "regional" routing and is assigned by the user.  The second character is uniformally randomly generated to yeild character values between 0..Z.  This random character can be used to route between various "shards" of data separated in upto 36 buckets (0..Z).  The bucket count is usually smaller in the range of 2, 3, 4, 6, 9, 12 or 18. Keys can be decoded to reveal route (region), shard and create date.

Example:

```
region = 1
key = UniqueKeys.shard_key(region)
"1W477BWWVSFT6W2YLUD41K"

UniqueKeys.decode_shard_key(key)
{:ok, [route: "1", shard: "W"], #DateTime<2019-06-03 16:15:48.742358Z>}
```

## Other Keys

Other keys incudeing UUID, GUID, and random bytes can be generated as follows:

```
UniqueKeys.create_txid()
"xhwh0tnexj2u2"

UniqueKeys.create_uuid()
"7bdf9554-97eb-d506-c709-cfc37815e0e5"

UniqueKeys.create_guid()
"dd10fc5738016f6ba916b0e2c656a16a"

UniqueKeys.create_bytes()
"4m0n1k533k2d6p4r4h106o1i1n5r482l0820731f6v110m3s"

size = 48
base = 16
UniqueKeys.create_bytes(size, base)
"22b77449cd3c8b567b3c356c6f39642ad9141885ed17d24c1bf517ad873019b7d508707abea6b05669cf95952bd4da83"

base = 36
UniqueKeys.create_bytes(size, base)
"3h256g1f001i6d226j3e2g6j36735g20090t6s636o0j50495u6k095j003t0b5d021e3p4s2r1v4d4a5c061c5w420e184t"
```

_UUID and GUID are version 4, time based_

## Crypto

Support for symmentric encryption / decryptioin using the new erlang (v23) crypto library.  Includes key generator and keystore support.

## Unit Tests

Unit tests are a great place to see example use.  Below are the results of a test run using `make test`:

```
UniqueKeysTest [test/unique_keys_test.exs]
  * test unique keys public API should encode a uuid (28.2ms) [L#26]
  * test unique keys public API version/0 (0.01ms) [L#6]
  * test unique keys public API should encode and decode a txid (6.1ms) [L#19]
  * test unique keys public API should encode and decode a random shard key (0.1ms) [L#41]
  * test unique keys public API should create a shard key (0.03ms) [L#51]
  * test unique keys public API should encode a guid (0.03ms) [L#31]
  * test unique keys public API should encode and decode a standard key (0.03ms) [L#12]
  * test unique keys public API should return a random byte string with a default length (0.05ms) [L#36]

KeysTest [test/keys_test.exs]
  * test create_txid/0 should create a txid (0.02ms) [L#21]
  * test create_key/0 should decode a standard key (0.02ms) [L#13]
  * test shard_key/2 should create a random shared key (0.05ms) [L#46]
  * test shard_key/2 should create a static sharded key (0.03ms) [L#56]
  * test create_txid/0 should decode the date time from a txid (0.02ms) [L#26]
  * test create_key/0 should create a key with a specified lenght (0.02ms) [L#8]
  * test create_txid/0 should not detect collisions over a large set (1555.3ms) [L#32]

UniqueKeysCryptoTest [test/crypto_test.exs]
  * test UniqueKeys.Crypto API encrypt/2 (6.7ms) [L#20]
  * test UniqueKeys.Crypto API decrypt/2 (0.03ms) [L#27]
  * test UniqueKeys.Crypto API generate_key/1 (0.01ms) [L#10]
  * test UniqueKeys.Crypto API generate_keystore/2 (24.4ms) [L#14]


Finished in 1.9 seconds
19 tests, 0 failures

Randomized with seed 972116

Generating cover results ...

Percentage | Module
-----------|--------------------------
   100.00% | UniqueKeys
   100.00% | UniqueKeys.Crypto
   100.00% | UniqueKeys.Keys
-----------|--------------------------
   100.00% | Total
```

###### darryl.west | 2020.11.03
