# Reference

## Keystore

A list of 10,000 keys used as an indexable refernce to K1 and K2 create a combination of 100,000,000 posible combinations.

## Keys

```

{idx1, k1} = KeyStore.random()
{idx2, k2} = KeyStore.random()

key = k1 ^^^ k2

idx = << idx1 >> << idx2 >>
```

###### darryl.west | 2020.11.06
